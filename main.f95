program main !**************************************************************************************
!***************************************************************************************************
!Investigation of Finite difference method error characteristics on a first order wave problem
!Author :: Md Abu Horaira Banna, Graduate student, University of Alabama(mbanna@crimson.ua.edu)
!Version 01.00: 10/12/2016
!***************************************************************************************************
!***************************************************************************************************

implicit none

!---------------------------------------------------------------------------------------------------
! Global variables
!---------------------------------------------------------------------------------------------------

    real, dimension(0:100) :: u, un, ua, y, er, a, b, c, d, g, h
    integer :: jm, i ,j, Nstep
    real    :: tmax, tstep, tstar, ymax, ystep, r, alpha, summ, u0, pi, time


!---------------------------------------------------------------------------------------------------
! time variables
!---------------------------------------------------------------------------------------------------
	tmax = 1.08     ! s, maximum time considered
    tstep = 0.002   ! s, time step size
	Nstep = int( tmax / tstep )


!---------------------------------------------------------------------------------------------------
! variables in Y direction
!---------------------------------------------------------------------------------------------------
	ymax = 0.04          ! m, maximum distance in Y direction
	ystep = 0.001 	! m , length of spatial segment
	jm =  ymax/ystep + 1     	    ! number of spatial nodes


	alpha = 0.000217 ! viscocity, m^2/sec
	r = alpha * tstep/ (ystep * ystep) ! diffusion number
	u0 = 40 ! m/s

!---------------------------------------------------------------------------------------------------
! Body
!---------------------------------------------------------------------------------------------------

	pi = 4.*atan(1.)

!---------------------------------------------------------------------------------------------------
! Mesh generation
!---------------------------------------------------------------------------------------------------

	do i = 0,jm-1
       y(i)=ystep*(i-1) !nodal positions in x direction
    end do

!---------------------------------------------------------------------------------------------------
! initialize
!---------------------------------------------------------------------------------------------------

    do i = 1, jm-2    !control loop to initialize value of disturbance (u) at time t=0
        un(i) = 0
    end do

!---------------------------------------------------------------------------------------------------
! Time loop
!---------------------------------------------------------------------------------------------------
		
	time = 0
    do i = 0,Nstep     ! time loop !!!!
        un(0) = 0
        un(jm-1) = 0
		time = time + tstep	
		
		call ftcs_explicit ()


		if (time == 0.18) then
			open(unit=11, file = 'ftcs_explicit_0.18.dat')
            do j=0,jm-1
                write(11,*) un(j), y(j)
            end do
			close(11)
		end if

		if (time == 0.36) then
			open(unit=11, file = 'ftcs_explicit_0.36.dat')
            do j=0,jm-1
                write(11,*) un(j), y(j)
            end do
			close(11)
		end if

		if (time == 0.54) then
			open(unit=11, file = 'ftcs_explicit_0.54.dat')
            do j=0,jm-1
                write(11,*) un(j), y(j)
            end do
			close(11)
		end if

		if (time == 0.72) then
			open(unit=11, file = 'ftcs_explicit_0.72.dat')
            do j=0,jm-1
                write(11,*) un(j), y(j)
            end do
			close(11)
		end if

		if (time == 0.9) then
			open(unit=11, file = 'ftcs_explicit_0.9.dat')
            do j=0,jm-1
                write(11,*) un(j), y(j)
            end do
			close(11)
		end if

		if (time == 1.08) then
			open(unit=11, file = 'ftcs_explicit_1.08.dat')
            do j=0,jm-1
                write(11,*) un(j), y(j)
            end do
			close(11)
		end if

    end do        ! end of time loop !!!

!---------------------------------------------------------------------------------------------------
! Exact solution
!---------------------------------------------------------------------------------------------------

	tstar = alpha*tmax/(ymax*ymax)
    do i = 0, jm-1
		summ = 0
		do j= 1, 10  !! ten terms are taken
			summ = summ + (1/j)*exp(-j*j*pi*pi*tstar)*sin(j*pi*y(i)/ymax)
		end do

		ua(i) = u0 * ( (1-y(i)/ymax) - (2/pi) * summ )
	end do
	open(unit=11, file = 'analytical_solution.dat')
    do i=0,jm-1
        write(11,*) ua(i), y(i)
    end do
	close(11)

!---------------------------------------------------------------------------------------------------
! Error calculation
!---------------------------------------------------------------------------------------------------

	do i = 0, jm-1
		er(i) = (ua(i)-un(i))*100/u0
	end do
	open(unit=11, file = 'error.dat')
    do i=0,jm-1
        write(11,*) er(i), y(i)
    end do
	close(11)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains !*****************************************************************************************
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    subroutine ftcs_explicit ()!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        do j = 1, jm-2
			u(j) = un(j) + r*( un(j-1) - 2*un(j) + un(j+1) )
		end do
		u(0) = u0
		u(jm-1) = 0;

		do j = 0, jm-1 !! repacking
			un(j) = u(j)
		end do

    end subroutine ftcs_explicit
	
	subroutine dufort_frankel () !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		real, dimension(0:100)	:: u1 
		
		un (0) = 40;
		un(jm-1) = 0;
		if (time == tstep) then
			call ftcs_explicit () ! calling ftcs expilicit for first time step
		end if
		
		do j = 1, jm-2
			u1(j) = (1/(1+2*r))*((1-2*r)*u(j)+(2*r)*(un(j+1) + un(j-1)));
		end do
		
		u(0) = u0
		u(jm-1) = 0
		
		do j = 0, jm-1  !! repacking
			u(j) = un(j) !!  u[j] stores u(n-1) values 
			un(j) = u1(j)
		end do
		
	end subroutine dufort_frankel !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	subroutine ftcs_implicit () !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		do j = 1, jm-2
            a(j) = -r
            b(j) = 1 + 2*r
            c(j) = -r
            d(j) = un(j)
        end do
		call tridiagonal ()
        
        u(0) = u0
        u(jm-1) = 0
        
		do j = 1, jm-1 !! repacking
            un(j) = u(j)
		end do
	end subroutine ftcs_implicit !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	subroutine crank_nicolson ()
		do j = 1, jm-2
            a(j) = -r/2
            b(j) = 1 + r
            c(j) = -r/2
            d(j) = un(j) + 0.5*r*(un(j-1)-2*un(j)+un(j+1))
        end do
		
        call tridiagonal ()
        
        u(0) = u0 !! repacking
        u(jm-1) = 0
        
		do j = 1, jm-1
            un(j) = u(j)
		end do
	end subroutine crank_nicolson  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
	subroutine tridiagonal () !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		integer :: n
        n = jm
		h(0) = 0
        g(0) = u(0)

        do j = 1,n-2
            h(j) = c(j)/(b(j) - a(j)*h(j-1))
            g(j) = (d(j) - a(j)*g(j-1))/(b(j) - a(j)*h(j-1))
        end do

        do j = n-2,1,-1
            u(j) = -g(j)*u(j+1) + g(j)
        end do
    end subroutine tridiagonal !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
end program main
